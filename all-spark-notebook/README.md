[![docker pulls](https://img.shields.io/docker/pulls/jupyter/all-spark-notebook.svg)](https://hub.docker.com/r/jupyter/all-spark-notebook/) [![docker stars](https://img.shields.io/docker/stars/jupyter/all-spark-notebook.svg)](https://hub.docker.com/r/jupyter/all-spark-notebook/) [![image metadata](https://images.microbadger.com/badges/image/jupyter/all-spark-notebook.svg)](https://microbadger.com/images/jupyter/all-spark-notebook "jupyter/all-spark-notebook image metadata")

# Jupyter Notebook Python, Scala, R, Spark, Mesos Stack

## Steps to setup the Jupyter Note Up and Running.

---

Step 1: Clone the Repository

`$ git clone https://AshokKumarChoppadandi@bitbucket.org/AshokKumarChoppadandi/jupyter-all-spark-notebooks.git`
 
Step 2: Go to **all-spark-notebook** directory

```$ cd all-spark-notebook```

Step 3: Run the Docker build command to build the Image from the Dockerfile.

```$ docker build . -t jupyter-spark-all-notebook```

NOTE: It will take sometime to download the dependent images when we for the first time(Be patient).

Step 4: Create a directory to store all your notebooks to our Host Machine instead of storing them in Container(Notebooks will get removed once the container stops).

```$ mkdir /home/vagrant/JupyterNotebooks```

Step 5: Launch the container.

```$ docker run --rm -d --mount src=/home/vagrant/JupyterNotebooks/,target=/home/jovyan/work/,type=bind -p 8888:8888 --name my_jupyter_test jupyter-spark-all-notebook```

Step 6. Open the browser and check at URL localhost:8888. Jupyter Notebook will get started. It will ask for the password.

Password :: **nielsen**

BOOOOOM...!!! We successfully setup the Jupyter Notebook for Python, Scala, R and Apache Spark.

**Happy Coding...!!!**

Reference Links:

* [Jupyter Docker Stacks on ReadTheDocs](http://jupyter-docker-stacks.readthedocs.io/en/latest/index.html)
* [Selecting an Image :: Core Stacks :: jupyter/all-spark-notebook](http://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-all-spark-notebook)
* [Image Specifics :: Apache Spark](http://jupyter-docker-stacks.readthedocs.io/en/latest/using/specifics.html#apache-spark)

This is the modified version of existing code in GitHub (https://github.com/jupyter/docker-stacks).